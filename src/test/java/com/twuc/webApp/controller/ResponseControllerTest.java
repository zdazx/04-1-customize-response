package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ResponseControllerTest {
    @Autowired
    private MockMvc mockMvc;

    // test section 2.1
    @Test
    void should_return_status_without_defining_status() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    // test section 2.2
    @Test
    void should_return_status_204() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    // test section 2.3
    @Test
    void should_verify_content_type_and_body() throws Exception {
        mockMvc.perform(get("/api/messages/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string("hello"));
    }

    // test section 2.4
    @Test
    void should_verify_message_object() throws Exception {
        mockMvc.perform(get("/api/message-objects/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"value\":\"hello\"}"));
    }

    // test section 2.5
    @Test
    void should_verify_status_when_method_with_annotation() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/hello"))
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"value\":\"hello\"}"));
    }

    // test section 2.6
    @Test
    void should_return_with_response_entity() throws Exception {
        mockMvc.perform(get("/api/message-entities/hello"))
                .andExpect(status().isCreated())
                .andExpect(header().string("X-Auth","me"))
                .andExpect(content().string("{\"value\":\"hello\"}"));
    }
}
