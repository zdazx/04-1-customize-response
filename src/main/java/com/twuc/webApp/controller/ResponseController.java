package com.twuc.webApp.controller;

import com.twuc.webApp.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseController {
    // test section 2.1
    @GetMapping("/api/no-return-value")
    public void returnStatusWhenMethodReturnNothing(){ }

    // test section 2.2
    // status 204 is no content
    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void returnStatus204WhenMethodReturnNothing(){}

    // test section 2.3
    @GetMapping("/api/messages/{message}")
    public String returnMessage(@PathVariable String message){
        return message;
    }

    // test section 2.4
    @GetMapping("/api/message-objects/{message}")
    public Message returnMessageObject(@PathVariable String message){
        return new Message(message);
    }

    // test section 2.5
    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message returnStatus202(@PathVariable String message){
        return new Message(message);
    }

    // test section 2.6
    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity returnWithResponseEntity(@PathVariable String message){
        return ResponseEntity.created(null)
                .header("X-Auth","me")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(new Message(message));
    }
}
